const { pipeline } = require('stream')
const { createWriteStream, createReadStream } = require('fs')
const JSONStream = require("JSONStream");
const { promisify } = require('util')
const zlib = require("zlib");
const pipelinePromise = promisify(pipeline);
const INPUT_FILE = './fake-data/list-us4liqaqhxravf7u5p1lcws0.json.gz'
const fs = require('fs')
const MAX_BATCH_FILES_SIZE_BYTES = 100000000
const MAX_BATCH_SIZE_ITEMS = 30


async function run() {
    try {

        await pipelinePromise(
            createReadStream(INPUT_FILE),
            zlib.createGunzip(),
            JSONStream.parse('*.attachments.*'),
            async function * attachmentTransformer(data) {
                let chunksCount = 0
                let batchIndex = 0
                let batchData = []
                let batchFileSizeBytes = 0
                const processBatch = () => {
                    if (batchData.length > 0) {
                        let filename = `attachments-${batchIndex}.json`
                        fs.writeFileSync(filename, JSON.stringify(batchData))
                        batchIndex++
                        batchData = []
                        batchFileSizeBytes = 0
                        return filename
                    }
                    return null
                }
                for await (let attachment of data) {
                    if (attachment.url && (typeof attachment.url === "string") && attachment.url.startsWith('https://trello.com/')) {
                        if (batchData.length >= MAX_BATCH_SIZE_ITEMS  ||
                            batchFileSizeBytes + attachment.bytes >= MAX_BATCH_FILES_SIZE_BYTES
                        ) {
                            let newFileName = processBatch()
                            if (newFileName) {
                                yield newFileName
                            }
                        }
                        batchData.push({
                            id: attachment.id,
                            name: attachment.name,
                            url: attachment.url,
                            bytes: attachment.bytes
                        })
                        batchFileSizeBytes += attachment.bytes
                        chunksCount++
                    }
                }
                let newFileName = processBatch()
                if (newFileName) {
                    yield newFileName
                }
                console.log(`+++attachments  added: ${chunksCount}`)
            },
            async function * results(files) {
                for await (let file of files) {
                    // await sqs.sendMessage({
                    //     MessageBody: JSON.stringify({file}),
                    //     QueueUrl
                    // }).promise()
                    console.log(file.toString())
                }
                console.log('+++++')
            })

    } catch (err) {
        console.log(err)
        process.exit(1)
    }
}


run()

