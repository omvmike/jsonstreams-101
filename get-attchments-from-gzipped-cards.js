const { pipeline } = require('stream')
const { createWriteStream, createReadStream } = require('fs')
const JSONStream = require("JSONStream");
const { promisify } = require('util')
const zlib = require("zlib");
const pipelinePromise = promisify(pipeline);
const INPUT_FILE = './fake-data/list-us4liqaqhxravf7u5p1lcws0.json.gz'
const OUTPUT_FILE = './attachments.json.gz'


async function run() {
    try {
        await pipelinePromise(
            createReadStream(INPUT_FILE),
            zlib.createGunzip(),
            JSONStream.parse('*.attachments.*'),
            async function * attachmentTransformer(data) {
                yield '['
                let i = 0
                for await (let attachment of data) {
                    if (attachment.url && (typeof attachment.url === "string") && attachment.url.startsWith('https://trello.com/')) {
                        //console.log(attachment)
                        const optionalComma = (i > 0) ? ',' : ''
                        yield optionalComma + JSON.stringify({
                            id: attachment.id,
                            name: attachment.name,
                            url: attachment.url
                        })
                        i++
                    }
                }
                console.log(`+++attachments  added: ${i}`)
                yield ']'
            },
            zlib.createGzip(),
            createWriteStream(OUTPUT_FILE)
            )
    } catch (err) {
        console.log(err)
        process.exit(1)
    }
}


run()

