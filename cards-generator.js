const { pipeline } = require('stream')
const { createWriteStream } = require('fs')
const { performance } = require('perf_hooks');
const faker = require('faker')
const LISTS_COUNT = 5
const CARDS_COUNT = 150
const RESULT_FILE = 'test-cards4.json'

var startTime = performance.now()
pipeline(
    function * generateData() {
        yield '{"lists":['
        const listIds = []
        for (i = 0; i < LISTS_COUNT; i++) {
            const optionalComma = (i > 0) ? ',' : ''
            const listData = newList()
            listIds.push(listData.id)
            yield optionalComma + JSON.stringify(listData)
        }
        yield '],"cards":['
        for (i = 0; i < CARDS_COUNT; i++) {
            const optionalComma = (i > 0) ? ',' : ''
            yield optionalComma + JSON.stringify(newCard(listIds))
        }
        yield ']}'
    },
    createWriteStream(__dirname + `/${RESULT_FILE}`),
    (err) => {
        if (err) {
            console.log('---oops',err)
        }
        var endTime = performance.now()
        const diff = endTime - startTime
        console.log(`+++finished in ${diff.toFixed(3)} ms`)
    }
)

function newAttachment(cardId) {
    const attachmentId = faker.random.alphaNumeric(24)
    const fileName = faker.system.fileName()
    return {
        "bytes": 12586219,
        "date": "2016-10-19T09:04:22.430Z",
        "edgeColor": null,
        "idMember": "553766b6e663ad9cd97269aa",
        "isUpload": true,
        "mimeType": null,
        "name": fileName,
        "previews": [],
        "url": `https://trello.com/1/cards/${cardId}/attachments/${attachmentId}/download/${fileName}`,
        "pos": 16384,
        "fileName": fileName,
        "id": attachmentId
    }
}

function getCardAttachments(cardId) {
    let res = []
    const num = Math.round(Math.random() * 3)
    for (let i = 0; i < num; i++) {
        res.push(newAttachment(cardId))
    }
    return res
}

function newCard(lists) {
    const cardId = faker.random.alphaNumeric(24)
    return {
        "id": cardId,
        "address": null,
        "checkItemStates": null,
        "closed": false,
        "coordinates": null,
        "creationMethod": null,
        "dateLastActivity": "2021-12-08T05:52:16.699Z",
        "desc": "",
        "descData": {
            "emoji": {}
        },
        "dueReminder": null,
        "idBoard": "60dc3130d4e24a7b50986e49",
        "idLabels": [],
        "idList": faker.random.arrayElement(lists),
        "idMembersVoted": [],
        "idShort": 1,
        "idAttachmentCover": null,
        "locationName": null,
        "manualCoverAttachment": false,
        "name": faker.lorem.words(5),
        "pos": 65535,
        "shortLink": "ezdjUCfZ",
        "isTemplate": false,
        "cardRole": null,
        "badges": {
            "attachmentsByType": {
                "trello": {
                    "board": 0,
                    "card": 0
                }
            },
            "location": false,
            "votes": 0,
            "viewingMemberVoted": false,
            "subscribed": false,
            "fogbugz": "",
            "checkItems": 0,
            "checkItemsChecked": 0,
            "checkItemsEarliestDue": null,
            "comments": 0,
            "attachments": 0,
            "description": false,
            "due": null,
            "dueComplete": false,
            "start": null
        },
        "dueComplete": false,
        "due": null,
        "email": "stanislavsyso+2mvrhlxdy6y301p5pqq+2xirvh8uu0n8lorcjcm+2q8wr7besy@boards.trello.com",
        "idChecklists": [],
        "idMembers": [],
        "labels": [],
        "limits": {
            "attachments": {
                "perCard": {
                    "status": "ok",
                    "disableAt": 1000,
                    "warnAt": 800
                }
            },
            "checklists": {
                "perCard": {
                    "status": "ok",
                    "disableAt": 500,
                    "warnAt": 400
                }
            },
            "stickers": {
                "perCard": {
                    "status": "ok",
                    "disableAt": 70,
                    "warnAt": 56
                }
            }
        },
        "shortUrl": "https://trello.com/c/ezdjUCfZ",
        "start": null,
        "subscribed": false,
        "url": "https://trello.com/c/ezdjUCfZ/1-some-card",
        "cover": {
            "idAttachment": null,
            "color": null,
            "idUploadedBackground": null,
            "size": "normal",
            "brightness": "dark",
            "idPlugin": null
        },
        "attachments": getCardAttachments(cardId),
        "pluginData": [],
        "customFieldItems": []
    }
}

function newList() {
    return {
        "id": faker.random.alphaNumeric(24),
        "name": faker.lorem.words(3),
        "closed": false,
        "pos": 196607,
        "softLimit": null,
        "creationMethod": null,
        "idBoard": "60dc3130d4e24a7b50986e49",
        "limits": {
            "cards": {
                "openPerList": {
                    "status": "ok",
                    "disableAt": 5000,
                    "warnAt": 4000
                },
                "totalPerList": {
                    "status": "ok",
                    "disableAt": 1000000,
                    "warnAt": 800000
                }
            }
        },
        "subscribed": false
    }
}
