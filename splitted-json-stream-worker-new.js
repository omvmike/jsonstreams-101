const { pipeline, Readable, PassThrough, once } = require('stream')
const { createWriteStream, createReadStream } = require('fs')
const { performance } = require('perf_hooks');
const JSONStream = require("JSONStream");
const { promisify } = require('util')
const zlib = require("zlib");
const INPUT_FILE = './test-cards4.json'

const timeout = promisify(setTimeout)
const pipelinePromise = promisify(pipeline);

var startTime = performance.now()
const lists = []
const writePipelinePromises = []
pipeline(
    createReadStream(INPUT_FILE),
    JSONStream.parse('lists.*'),
    async function * listWriter(jsonData) {
        for await (let chunk of jsonData) {
           //console.log('---', chunk.id)
            lists[chunk.id] = {
                data: chunk,
                inputStream: null,
                isPipelineCreated: false,
                asyncWrite: null
            }
        }
        var endTime = performance.now()
        const diff = endTime - startTime
        console.log(`++done listWriter in ${diff.toFixed(3)} ms`, Object.keys(lists).length)
        await pipelinePromise(
            createReadStream(INPUT_FILE),
            JSONStream.parse('cards.*'),
            async function * cardSplitter(jsonData) {
                let counter = 0;
                for await (let card of jsonData) {
                    counter++
                    let appendChunk = true;
                    if (!lists[card.idList].isPipelineCreated) {
                        let newWritePipelinePromise = createPipeline(card.idList)
                        writePipelinePromises.push(newWritePipelinePromise)
                        lists[card.idList].isPipelineCreated = true
                        appendChunk = false
                    }
                    const optionalComma = appendChunk ? ',' : ''
                    await lists[card.idList].asyncWrite(optionalComma + JSON.stringify(card), 'utf8')
                }
                Object.keys(lists).map((k) => {
                    if (lists[k].isPipelineCreated) {
                        lists[k].inputStream.end()
                    }
                })
                if (writePipelinePromises.length) {
                    await Promise.all(writePipelinePromises)
                }
                console.log('++done cardSplitter', counter)
            })
    },
    (err) => {
        if (err) {
            console.log('---oops',err)
        }
        var endTime = performance.now()
        const diff = endTime - startTime
        console.log(`+++finished in ${diff.toFixed(3)} ms`)
    }
)




function createPipeline(listId) {
    lists[listId].inputStream = new PassThrough()
    const destinationPath = `./fake-data/list-${listId}.json.gz`
    const writeStream = createWriteStream(destinationPath)
    lists[listId].asyncWrite = buildWrite(lists[listId].inputStream)
    return  pipelinePromise(
        lists[listId].inputStream,
        async function * transformToCardsJson(data) {
            yield '['
            let i = 0
            for await (let chunkBuffer of data) {
                yield chunkBuffer.toString('utf8')
                //console.log(cardBuffer.length)
                //await timeout(100)
                i++
            }
            yield ']'
            console.log(`Prepared file ${destinationPath} of ${i} chunks`)
        },
        zlib.createGzip(),
        writeStream
    )
}

function buildWrite(stream) {
    let streamError = null
    stream.on('error', function (err) {
        streamError = err
    })
    return write
    function write(chunk) {
        if (streamError) {
            return Promise.reject(streamError)
        }
        const res = stream.write(chunk)
        if (res) {
            return Promise.resolve()
        }
        console.log('-- waiting for drain')
        return once(stream, 'drain')
    }
}
