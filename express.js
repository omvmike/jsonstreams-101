const express = require('express')
const fs = require('fs')
const app = express()
const port = 3010

app.get('/', (req, res) => {
    res.set('Content-Type', 'application/json')
    return res.send(fs.readFileSync(__dirname + '/test-data2.json').toString());
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
