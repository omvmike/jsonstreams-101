const { pipeline, Readable } = require('stream')
const { createWriteStream, createReadStream } = require('fs')
const { performance } = require('perf_hooks');
const JSONStream = require("JSONStream");
const { promisify } = require('util')
const INPUT_FILE = './test-cards3.json'
const CARD_BUFFER_LENGTH = 1000

const pipelinePromise = promisify(pipeline);

var startTime = performance.now()
const lists = []
pipeline(
    createReadStream(INPUT_FILE),
    JSONStream.parse('lists.*'),
    async function * listWriter(jsonData) {
        for await (let chunk of jsonData) {
           //console.log('---', chunk.id)
            lists[chunk.id] = {
                data: chunk,
                cardsBuffer: [],
                bufferIndex: 0,
            }
        }
        var endTime = performance.now()
        const diff = endTime - startTime
        console.log(`++done listWriter in ${diff.toFixed(3)} ms`, Object.keys(lists).length)
        await pipelinePromise(
            createReadStream(INPUT_FILE),
            JSONStream.parse('cards.*'),
            async function * cardSplitter(jsonData) {
                let counter = 0;
                for await (let card of jsonData) {
                    counter++
                    lists[card.idList].cardsBuffer.push(card)
                    if (lists[card.idList].cardsBuffer.length > CARD_BUFFER_LENGTH) {
                        await writeBufferToFile(card.idList)
                    }
                }
                // finalize buffers writes
                for (let listId of Object.keys(lists)) {
                    await writeBufferToFile(listId, true)
                }
                console.log('++done cardSplitter', counter)
            })

    },
    (err) => {
        if (err) {
            console.log('---oops',err)
        }
        var endTime = performance.now()
        const diff = endTime - startTime
        console.log(`+++finished in ${diff.toFixed(3)} ms`)
    }
)


async function writeBufferToFile(listId, isFinalBatch = false) {
    const destinationPath = `./fake-data/list-${listId}.json`
    const isFirstBatch = lists[listId].bufferIndex === 0
    const writeStream = isFirstBatch
        ? createWriteStream(destinationPath)
        : createWriteStream(destinationPath, {flags: 'a'})
    writeStream.on('error', (err) => {
        console.log('---writeBufferToFile',err)
        throw new Error(err);
    })
    await pipelinePromise(
        function * reader() {
            if (isFirstBatch) {
                yield '{"cards":['
            }
            for (i = 0; i < lists[listId].cardsBuffer.length; i++) {
                const optionalComma = (i > 0) || !isFirstBatch ? ',' : ''
                yield optionalComma + JSON.stringify(lists[listId].cardsBuffer[i])
            }
            // flush buffer
            lists[listId].cardsBuffer = []
            lists[listId].bufferIndex++
            // finalize
            if (isFinalBatch) {
                yield ']}'
            }
        },
        writeStream
    )
}

// async function finalizeFile(listId) {
//     const destinationPath = `./fake-data/list-${listId}.json`
//     const isFirstBatch = lists[listId].bufferIndex === 0
//     const writeStream = isFirstBatch
//         ? createWriteStream(destinationPath)
//         : createWriteStream(destinationPath, {flags: 'a'})
//     writeStream.on('error', (err) => {
//         console.log('---finalizeListFile',err)
//         throw new Error(err);
//     })
//     await pipelinePromise(
//         function * reader() {
//             if (isFirstBatch) {
//                 yield '{"cards":['
//             }
//             for (i = 0; i < lists[listId].cardsBuffer.length; i++) {
//                 const optionalComma = (i > 0) || !isFirstBatch ? ',' : ''
//                 yield optionalComma + JSON.stringify(lists[listId].cardsBuffer[i])
//             }
//             yield ']}'
//             // flush buffer
//             lists[listId].cardsBuffer = []
//             lists[listId].bufferIndex++
//         },
//         writeStream
//     )
// }


// class Runner {
//     constructor() {
//         this.pipeline = promisify(pipeline)
//     }
//
//     register(steps) {
//         this.steps = steps
//         return this
//     }
//
//     run() {
//         return this.pipeline(...this.steps)
//     }
// }
