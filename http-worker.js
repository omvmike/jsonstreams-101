var axios = require('axios');
const { pipeline, Readable } = require('stream')
const {performance} = require("perf_hooks")
const JSONStream = require("JSONStream")
const {createWriteStream} = require("fs")

var startTime = performance.now()

const destinationPath = `./fake-data/cards.json.gz`
const writeStream = createWriteStream(destinationPath)

var zlib = require('zlib');
var gz = zlib.createGzip();

axios({
    method: 'get',
    url: 'http://localhost:3010',
    responseType: 'stream'
}).then(function (res) {
    pipeline(
        res.data,
        JSONStream.parse('cards.*'),
        async function * cardsTransform(data) {
            yield '{"cards":['
            let counter = 0;
            for await (let chunk of data) {
                const optionalComma = (counter > 0) ? ',' : ''
                yield optionalComma + JSON.stringify(chunk)
                counter++
            }
            yield ']}'
            console.log('+++added ' + counter)
        },
        gz,
        writeStream,
        (err) => {
            if (err) {
                console.log('---oops',err)
            }
            var endTime = performance.now()
            const diff = endTime - startTime
            console.log(`+++finished in ${diff.toFixed(3)} ms`)
        }
    )
})

