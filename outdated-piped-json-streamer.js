const fs = require('fs');
const JSONStream = require('JSONStream')
const es = require('event-stream')

const lists = [];

const { performance } = require('perf_hooks');
var startTime = performance.now()
const processLists = async () => new Promise(function(resolve, reject) {
    fs.createReadStream('./test-cards.json')
        .on("error", reject)
        .pipe(JSONStream.parse('lists.*'))
        .pipe(es.through(function write(data) {
            console.error(`-name=${data.name}-\n`);
            try {
                lists[data.id] = {
                    data,
                    file: fs.createWriteStream(`./list-${data.id}`).on('error', () => console.log('oops'))
                };
            } catch (e) {
                console.log('oops');
                reject
            }
        }, function end() { //optional
            console.error("---end-\n");
            resolve(lists);
        }));
})
// a = fs.createWriteStream(`./list-${data.id}`)
// a.write()
const processCards = async () => new Promise(function(resolve, reject) {
    fs.createReadStream('./test-cards.json')
        .on("error", reject)
        .pipe(JSONStream.parse('cards.*'))
        .pipe(es.through(function write(data) {
            //console.error(`-name=${data.name}-\n`);
            this.pause();
            try {
                //console.error(`++${data.idList}\n`);
                lists[data.idList].file.write(JSON.stringify(data), 'UTF8');
                this.resume();
            } catch (e) {
                reject(e)
            }

        }, function end() { //optional
            console.error("---end-\n");
            resolve();
        }));
})

const run = async () => {
    const r = await processLists();
    if (r) {
        await processCards();
        lists.map(list => list.file.close());
        var endTime = performance.now()
        const diff = endTime - startTime
        console.log(`+++finished in ${diff.toFixed(3)} ms`)
    }

    //console.log(r)
}

run()
