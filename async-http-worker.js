var axios = require('axios');
const { pipeline, Readable, Writable, PassThrough } = require('stream')
const {performance} = require("perf_hooks")
const JSONStream = require("JSONStream")
const {createWriteStream} = require("fs")
const zlib = require("zlib");
const { promisify } = require('util')
const pipelinePromise = promisify(pipeline);
const BUCKET= "calendly-dev-uploads"
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    signatureVersion: 'v4',
})


async function run() {
    var startTime = performance.now()
    const destinationPath = `fake-data/cards.json.gz`
    try {
        const {writeStream, uploadPromise} = uploadS3Stream(destinationPath)
        var gz = zlib.createGzip();
        const respStream = await axios({
            method: 'get',
            url: 'http://localhost:3010',
            responseType: 'stream'
        })
        let cardsCounter = 0;
        await pipelinePromise(
            respStream.data,
            // JSONStream.parse('cards.*'),
            // async function* cardsTransform(data) {
            //     yield '{"cards":['
            //     for await (let chunk of data) {
            //         const optionalComma = (cardsCounter > 0) ? ',' : ''
            //         yield optionalComma + JSON.stringify(chunk)
            //         cardsCounter++
            //     }
            //     yield ']}'
            //     console.log('+++added ' + cardsCounter)
            // },
            gz,
            logger,
            writeStream
        )
        await uploadPromise
        var endTime = performance.now()
        const diff = endTime - startTime
        console.log(`+++finished in ${diff.toFixed(3)} ms`)
        console.log('results file ' + destinationPath)
        return cardsCounter
    } catch (err) {
        console.log('---oops',err)
        return null
    }
}

function uploadS3Stream(key) {
    var writeStream = new PassThrough();
    const uploadPromise = s3.upload({
        Bucket: BUCKET,
        Key: key,
        Body: writeStream
    }).promise();
    return {uploadPromise, writeStream};
}


async function* logger(data) {
    let i = 0;
    for await (let chunk of data) {
        console.log('logger chunk',i++)
        yield chunk
    }
}

run()
