var axios = require('axios');
const { pipeline, Readable, Writable, PassThrough } = require('stream')
const {performance} = require("perf_hooks")
const JSONStream = require("JSONStream")
const {createWriteStream} = require("fs")
const zlib = require("zlib");
const { promisify } = require('util')
const pipelinePromise = promisify(pipeline);
const BUCKET= "calendly-dev-uploads"
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    signatureVersion: 'v4',
})


async function run() {
    var startTime = performance.now()
    const destinationPath = `./fake-data/trello-attachments.json.gz`
    try {
        const writeStream = createWriteStream(destinationPath)//uploadS3Stream(destinationPath)
        var gz = zlib.createGzip();
        const respStream = await axios({
            method: 'get',
            url: 'http://localhost:3010',
            responseType: 'stream'
        })
        let cardsCounter = 0;
        await pipelinePromise(
            respStream.data,
            JSONStream.parse('cards.*.attachments.*'),
            async function* cardsTransform(data) {
                yield '{"attachments":['
                for await (let chunk of data) {
                    if (chunk.url && (typeof chunk.url === "string") && chunk.url.startsWith('https://trello.com/')) {
                        console.log(chunk)
                        const optionalComma = (cardsCounter > 0) ? ',' : ''
                        yield optionalComma + JSON.stringify(chunk)
                        cardsCounter++
                    }
                }
                yield ']}'
                console.log('+++added ' + cardsCounter)
            },
            gz,
            writeStream
        )
        var endTime = performance.now()
        const diff = endTime - startTime
        console.log(`+++finished in ${diff.toFixed(3)} ms`)
        console.log('results file ' + destinationPath)
        return cardsCounter
    } catch (err) {
        console.log('---oops',err)
        return null
    }
}

function uploadS3Stream(key) {
    var pass = new PassThrough();
    s3.upload({
        Bucket: BUCKET,
        Key: key,
        Body: pass
    });
    return pass;
}

run()
